<?php
namespace frontend\models;

use backend\models\AuthItems;
use backend\models\AuthAssignment;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $username;
    public $email;
    public $password;
    public $permissions;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required','message'=>'Fundnel Says : You must fill this field !'],
            ['firstname', 'required','message'=>'Fundnel Says : You must fill this field !'],
            ['lastname', 'required','message'=>'Fundnel Says : You must fill this field !'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();

            // add permissions
            
            $permissionsList = $_POST['SignupForm']['permissions'];
            if (is_array($permissionsList) || is_object($permissionsList)){
                foreach ($permissionsList as $value) {
                    $newPermission = new AuthAssignment;
                    $newPermission->user_id = $user->id;
                    $newPermission->item_name = $value;
                    $newPermission->save();
                }
            }

            if ($user->save()) {
                return $user;
            }

        }

        return null;
    }
}

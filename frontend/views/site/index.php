<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>



    </div>
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>NO. #1 <br/>Online Game Portal</h1>
                <hr>
                <p>FUNDGAMES.COM</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">The best Games Review in the world !</h2>
                    <hr class="light">
                    <p class="text-faded">Fund Games has everything you need to get all information you need for all Trending games in the world. Be the part of us by create your review or rating games !</p>
                    <a href="index.php/site/login" class="btn btn-default btn-xl">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">

            <?php  
                foreach ($dataProvider->models as $model) {     
                    echo "<div class='col-lg-4 col-sm-6'><a href='".$backendlink."index.php/review/details?id={$model->idpost}' class='portfolio-box'>";
                    if($model->img==''){
                        echo "<img src='".$backendlink."img/noimages.jpg' class='img-responsive' alt='' style='height:222px;width:100%'>";
                    }
                    else{
                        echo "<img src='".$backendlink."{$model->img}' class='img-responsive' alt='' style='height:222px;width:100%'>";                        
                    }
                    echo "<div class='portfolio-box-caption'>
                                    <div class='portfolio-box-caption-content'>
                                        <div class='project-category text-faded'>
                                            {$model->subject}
                                        </div>
                                        <div class='project-name'>
                                            Project Name
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>";
                } 
            ?>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Be the first Reviewer for You Favorite Games!</h2>
                <a href="index.php/site/signup" class="btn btn-default btn-xl wow tada">Sign Up Now!</a>
            </div>
        </div>
    </aside>

    <style type="text/css">
    .navbar-inverse{background: transparent;
    border:0;}
    </style>

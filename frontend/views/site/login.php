<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'LOGIN FORM';
//$this->params['breadcrumbs'][] = $this->title;
?>
    </div>
    <aside class="bg-dark" style="background-image: url(../../css/bootstrap/img/portfolio/7.jpg);">
        <div class="container text-center">

                    <div class="row">
                    <h1><?= Html::encode($this->title) ?></h1>

                        <div class="col-lg-1"></div>
                        <div class="col-lg-5" style="background:rgba(34,34,34,0.6);margin-right:20px;height:400px"><br/><br/>
                        <p>Please fill out the following fields to login:</p>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                                <?= $form->field($model, 'username') ?>

                                <?= $form->field($model, 'password')->passwordInput() ?>

                                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>

                                <div class="form-group">
                                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-xl wow tada', 'name' => 'login-button']) ?>
                                </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                        <div class="col-lg-5" style="background:rgba(34,34,34,0.6);height:400px"><br/><br/><br/><br/><br/>
                            <p>Don't have account ?</p><br/>
                            <h1><a href="signup" class=" wow tada">REGISTER<br/>NOW !</a></h1>
                        </div>
                    </div>
                </div>
    </aside>






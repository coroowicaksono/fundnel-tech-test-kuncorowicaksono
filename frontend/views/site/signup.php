<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'SIGNUP';
//$this->params['breadcrumbs'][] = $this->title;
?>
    </div>
    <aside class="bg-dark" style="background-image: url(../../css/bootstrap/img/portfolio/8.jpg);">
        <div class="container text-center">

                    <div class="row">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8" style="text-align:left;background:rgba(34,34,34,0.6);padding:40px 50px;">
                            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                                <?= $form->field($model, 'firstname') ?>

                                <?= $form->field($model, 'lastname') ?>

                                <?= $form->field($model, 'username') ?>

                                <?= $form->field($model, 'email') ?>

                                <?= $form->field($model, 'password')->passwordInput() ?>

                                <?php 
                                    $authItems = ArrayHelper::map($authItems,'name','name')
                                ?>
                                <?= $form->field($model, 'permissions')->
                                checkboxList($authItems,[
                                   'item' =>
                                        function ($index, $label, $name, $checked, $value) {
                                             return Html::checkbox($name, $checked, [
                                             'value' => $value,
                                             'label' => '<label for="' . $label . '">' . $label . ' </label>',
                                             'labelOptions' => [
                                                'class' => 'ckbox ckbox-primary checkbox-inline',
                                             ],
                                            'id' => $label,
                                            'class' => 'any class',
                                       ]);
                                   }
                              ]); ?>

                                <div class="form-group"><br/>
                                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary btn-xl wow tada', 'name' => 'signup-button']) ?>
                                </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
    </aside>

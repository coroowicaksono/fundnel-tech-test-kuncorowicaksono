# FUNDGAMES - AN ONLINE GAME PORTAL #

Hello There.

This project is based on task for me (6 January 2016 at 11:00 by Lawrence Koh).

INSTALLATION 
===============================
For instalation please follow this intruction :

• 1. Download all this repository to your local repository (which contain localhost)
 
• 2. After copy all the data, import **fundgameswithreview.sql** or **fundgames.sql** (if you need a truncate one) to your database (located on fundgames/fundgameswithreview.sql or fundgames/fundgames.sql)
 
• 3. To go to homepage, please use this link : [http://localhost/fundgames/frontend/web/index.php](Link URL)
 
• 4. Note : If you copy those repository to another folder, please follow this next step : 

please change some code in fundgames/backend/config/main.php       
```
#!php

'urlManagerFrontEnd' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://localhost/fundgames/frontend/web/index.php', // change this code based on your root folder
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
```

and code in fundgames/backend/config/main.php  
        
```
#!php

'urlManagerBackEnd' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://localhost/fundgames/backend/web/', // change this code based on your root folder
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
```

TASK DESCRIPTION
===============================
• Guest can view game details and read related comments [Done]

• Guest can login to existing account [Done]

• Guest can register new account [Done]

• Guest can view a list of games and click on any to view details [Done]

• Login users enjoy the same privilege as Guest [Done]

• Login users can create new game page [Done]

• Login users can comment on game page [Done]

• Login users can update game page [Done]


TASK REQUIREMENT :
===============================
• Yii 2 framework [Done]

• MySQL [Done]

TASK OPTIONAL :
===============================
• RBAC [Done] - I deliberately to put access to post reviews and comments on the signup page to show RBAC function

• Login users can only update their own game page [Done] - You can take a look by using another user (user 2) to view review that created by user 1.

• Search, filter and sort game list [Not yet]

• Upload game cover [Done]

• Nice URL [Done] - I change from index.php?r= to index.php/

• Customized UI [Done] - I'm using free bootstrap template with some configuration

• Responsive Design [Done] - I have tested on different size of devices.

• Migration [Done]

• Unit testing [Done] - I tested this application more than 5 times.

• Anything extra - I'm adding slider to the page after user login. I actually want to include some charts to describe the profile of the Login user, but with great regret I must say that I'm not yet able to add it


LAST, BUT NOT LEAST
===============================

That's all that I'm doing until now. First, thankyou for giving me chance to get interview, but actually I learn so much caused of this task, so Thankyou so much again.


Best Regards,

Kuncoro Wicaksono Adi Baroto
<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "comment".
 *
 * @property integer $idcomment
 * @property integer $id_review
 * @property integer $id_usercomment
 * @property string $commentdesc
 * @property string $commenton
 *
 * @property User $idUsercomment
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_review', 'id_usercomment', 'commentdesc', 'commenton'], 'required'],
            [['id_review', 'id_usercomment'], 'integer'],
            [['commentdesc'], 'string'],
            [['commenton'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idcomment' => 'Idcomment',
            'id_review' => 'Id Review',
            'id_usercomment' => 'Id Usercomment',
            'commentdesc' => 'Commentdesc',
            'commenton' => 'Commenton',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsercomment()
    {
        return $this->hasOne(User::className(), ['id' => 'id_usercomment']);
    }
}

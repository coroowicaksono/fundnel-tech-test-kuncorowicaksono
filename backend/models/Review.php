<?php

namespace app\models;

use Yii;
use common\models\User;
use backend\models\Category;

/**
 * This is the model class for table "review".
 *
 * @property integer $idpost
 * @property string $subject
 * @property string $img
 * @property string $dsc
 * @property integer $id_author
 * @property string $createon
 * @property string $status
 *
 * @property User $idAuthor
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $file;

    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'status'], 'required'],
            [['id_category'], 'integer'],
            [['createon'], 'safe'],
            [['status'], 'string'],
            [['file'], 'safe'], //public function rules()
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions'=>'jpg, gif, png','checkExtensionByMimeType'=>false,'maxSize' => 1024 * 1024 * 2],
            [['subject'], 'string', 'max' => 155],
            [['img'], 'string', 'max' => 200],
            [['dsc'], 'string', 'max' => 5555]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idpost' => 'Idpost',
            'subject' => 'Subject',
            'img' => 'Img',
            'dsc' => 'Review Details',
            'file' => 'Images',
            'id_author' => 'Author',
            'createon' => 'Createon',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'id_author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['idcategory' => 'id_category']);
    }

    
}

<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Comment;

/**
 * SearchComment represents the model behind the search form about `backend\models\Comment`.
 */
class SearchComment extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idcomment', 'id_review', 'id_usercomment'], 'integer'],
            [['commentdesc', 'commenton'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idcomment' => $this->idcomment,
            'id_review' => $this->id_review,
            'id_usercomment' => $this->id_usercomment,
            'commenton' => $this->commenton,
        ]);

        $query->andFilterWhere(['like', 'commentdesc', $this->commentdesc]);

        return $dataProvider;
    }

    public function searchforeview($params)
    {
        $query = Comment::find()->select(['*'])->leftJoin('user', 'user.id = comment.id_usercomment')->andFilterWhere(['id_review' => $params]);
        //echo $query->createCommand()->sql;
        //die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'commenton' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        //print_r($dataProvider);

        return $dataProvider;
    }
}

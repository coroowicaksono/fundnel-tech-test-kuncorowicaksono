<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Review;

/**
 * SearchReview represents the model behind the search form about `app\models\Review`.
 */
class SearchReview extends Review
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idpost', 'id_author'], 'integer'],
            [['subject', 'img', 'dsc', 'createon', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idpost' => $this->idpost,
            'id_author' => $this->id_author,
            'createon' => $this->createon,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'dsc', $this->dsc])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public function indexsearch($params)
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idpost' => $this->idpost,
            'id_author' => $this->id_author,
            'createon' => $this->createon,
            'status' => 'Active',
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'dsc', $this->dsc])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public function sidebarsearch($params)
    {
        $query = Review::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
            'sort' => [
                'defaultOrder' => [
                    'createon' => SORT_ASC,
                    'subject' => SORT_ASC, 
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idpost' => $this->idpost,
            'id_author' => $this->id_author,
            'createon' => $this->createon,
            'status' => 'Active',
        ]);

        $query->andFilterWhere(['!=', 'idpost', $this->idpost])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'dsc', $this->dsc])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\FundBackendBootstrapAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\ActiveForm;

FundBackendBootstrapAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<body id="page-top">
<?php $this->beginBody() ?>


    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">

            <?php
            NavBar::begin([
                'brandLabel' => 'FUNDGAMES',
                'brandUrl' => Yii::$app->urlManagerFrontEnd->createUrl(''),
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top'
                ],
            ]);

            echo "<button class='form-control navbar-form navbar-right desktop' role='button' data-toggle='collapse' href='#collapseExample' aria-expanded='false' aria-controls='collapseExample'>
                  <i class='glyphicon glyphicon-search '></i>
                                        
                </button>
                    <form class='navbar-form navbar-right collapse mobile' role='search' id='collapseExample'>
                                   <div class='form-group has-feedback'>
                                        <input id='searchbox' type='text' placeholder='Search' class='form-control'>
                                        <span id='searchicon' class='glyphicon glyphicon-search form-control-feedback'></span>
                                    </div>
                              </form>";

            if (Yii::$app->user->isGuest) {
                $menuItems = [
                    ['label' => 'Home', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('')],
                    ['label' => 'About', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/about')],
                    ['label' => 'Contact', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/contact')],
                    ['label' => 'Signup', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/signup')],
                    ['label' => 'Login', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/login')],
                ];
            } else {
                $menuItems = [
                    ['label' => 'Home', 'url' => Yii::$app->urlManager->createUrl('')],
                    ['label' => 'About', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/about')],
                    ['label' => 'Contact', 'url' => Yii::$app->urlManagerFrontEnd->createUrl('/site/contact')],
                ];
                $menuItems[] = ['label' => 'Create Review', 'url' => ['/review/create']];
                $menuItems[] = [
                    'label' => Yii::$app->user->identity->firstname,
                    'items' => [[
                        'label' => 'Setting',
                        'url' => Yii::$app->urlManager->createUrl('/user/update?id='.Yii::$app->user->identity->id)
                    ],[
                        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems
            ]);
            NavBar::end();
            ?>
            <div class="navbar-header">
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



    <section class="bg-primary" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h3>Contact Us !</h3>
                    <hr>
                    <p>Help us to improve what you need by a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p><a href="#" class="btn btn-primary btn-xl page-scroll">021-xxx-xxxxx-xxx</a></p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a class="btn btn-primary btn-xl page-scroll" href="mailto:your-email@your-domain.com">games@fund.com</a></p>
                </div>
            </div>
        </div>
    </section>



<footer class="footer bg-dark" style="padding:10px">
    <div class="container">
        <p class="pull-left" style="font-family:'Open Sans';font-size:12px">&copy; FUNDGAMES <?= date('Y') ?></p>

        <p class="pull-right" style="font-family:'Open Sans';font-size:12px"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

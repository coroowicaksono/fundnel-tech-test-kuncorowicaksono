<?php

use yii\helpers\Html;
use yii\web\View;
//use yii\grid\ListView;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$jsView = file_get_contents(Yii::getAlias('@webroot/css/slider/unslidershown.js'));
$this->registerJs($jsView, View::POS_END);

?>


    </div>
    <header>
        <div class="header-content">
            <div class="header-content-inner" style="text-align:left">
                <div class="row">
                    <div class="col-lg-4 col-sm-12">
                        <h1>Hello,<br/><?= Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->lastname ?></h1>
                        <br/>
                         <?= Html::a('New Review', ['/review/create'], ['class' => 'btn btn-primary btn-xl page-scrol']) ?>
                    </div>
                    <div class="col-lg-8 col-sm-12">
                        <span>Latest Review by You :</span>
                        <div class="my-slider">
                            <ul>
                            <?php  
                                $showone = 1;
                                foreach ($dataProvider->models as $model) { 
                                    if($model->id_author==Yii::$app->user->identity->id){ 
                                        echo "<li class='text-center'><a href='review/details?id={$model->idpost}'>";
                                        if($model->img==''){
                                            echo "<img src='../img/noimages.jpg' class='img-responsive' alt='' style='width:100%'>";
                                        }
                                        else{
                                            echo "<img src='../{$model->img}' class='img-responsive' alt='' style='width:100%;max-height:350px'>";                        
                                        }
                                        echo "<div class='' style='background: rgba(0, 0, 0, .5);width:100%;font-family:Helvetica;text-decoration:none'>{$model->subject}</div></a></li>";
                                    }
                                    else{
                                        if($showone==1){
                                            echo "<li class='text-center'><a href='review/create'>";
                                            echo "<img src='../img/noreview.jpg' class='img-responsive' alt='' style='width:100%'>";
                                            echo "</a></li>";
                                        }
                                    }
                                $showone++;
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div> 

            </div>
        </div>
    </header>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">

            <?php  
                foreach ($dataProvider->models as $model) {     
                    echo "<div class='col-lg-4 col-sm-6'><a href='review/details?id={$model->idpost}' class='portfolio-box'>";
                    if($model->img==''){
                        echo "<img src='../img/noimages.jpg' class='img-responsive' alt='' style='height:222px;width:100%'>";
                    }
                    else{
                        echo "<img src='../{$model->img}' class='img-responsive' alt='' style='height:222px;width:100%'>";                        
                    }
                    echo "<div class='portfolio-box-caption'>
                                    <div class='portfolio-box-caption-content'>
                                        <div class='project-category text-faded'>
                                            {$model->subject}
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>";
                } 
            ?>
            </div>
        </div>
    </section>


    <style type="text/css">
    .navbar-inverse{background: transparent;
    border:0;}
    </style>

   
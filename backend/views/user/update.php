<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

//$this->title = 'Update User: ' . ' ' . $model->id;
echo "<h1>User Setting</h1>";
if(isset($done)){echo "<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong> Updated !</strong> Yeah, your account information has been updated.</div>";}else{}
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true])->label('Games Title') ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'dsc')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]); ?>

    <?= $form->field($model, 'id_author')->textInput(['readonly' => true, 'value' =>  Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->lastname]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Draft' => 'Draft', 'Active' => 'Active', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    <script>
        CKEDITOR.replace( 'Review[dsc]' );
    </script>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Review */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpost, 'url' => ['view', 'id' => $model->idpost]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="review-update">

    <?= Html::a('Cancel', ['details', 'id' => $model->idpost], [
        'class' => 'btn btn-danger pull-right',
        'data' => [
            'confirm' => 'Are you sure to cancel update this review ?',
        ],
    ]) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

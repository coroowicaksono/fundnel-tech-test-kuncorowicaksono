<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelcomment backend\modelcomments\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelcomment, 'id_review')->textInput()
  ->hiddenInput(['value' => $idpost]) ?>

    <?= $form->field($modelcomment, 'id_usercomment')->textInput() ?>

    <?= $form->field($modelcomment, 'commentdesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($modelcomment, 'commenton')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($modelcomment->isNewRecord ? 'Create' : 'Update', ['class' => $modelcomment->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

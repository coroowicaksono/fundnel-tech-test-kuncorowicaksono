<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\bootstrap\modal;


/* @var $this yii\web\View */
/* @var $model app\models\Review */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!--<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idpost], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idpost], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idpost',
            'subject',
            'img',
            'dsc',
            'id_author',
            'createon',
            'status',
        ],
    ]) ?>
--><br/>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">

            <?php  
                
                    echo "<div class='row'>
                    <div class='col-lg-4 col-sm-12'><a href='index.php?r=review%2Fdetail&id={$model->idpost}' class='portfolio-box'>";
                    if($model->img==''){
                        echo "<img src='../../img/noimages.jpg' class='img-responsive' alt='' style='width:100%'>";
                    }
                    else{
                        echo "<img src='../../{$model->img}' class='img-responsive' alt='' style='width:100%'>";                        
                    }
                    echo "<div class='portfolio-box-caption'>
                                    <div class='portfolio-box-caption-content'>
                                        <div class='project-category text-faded'>
                                            {$model->idCategory->categoryname}
                                        </div>
                                        <div class='project-name'>
                                            {$model->subject}
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class='row'>
                                <hr>
                                <div class='col-lg-6 col-sm-6 text-center'>
                                    <p style='font-family:arial;font-size:12px'><i class='glyphicon glyphicon-time'></i> {$model->createon}</p>
                                </div>
                                <div class='col-lg-6 col-sm-6 text-center'>
                                    <p style='font-family:arial;font-size:12px'><i class='glyphicon glyphicon-user'></i> {$model->idAuthor->firstname} {$model->idAuthor->lastname}</p>
                                </div>
                            </div>
                            <div class='row' style='margin-top:100px'>
                                <div class='col-lg-12 col-sm-4'>
                                    <h2>You May Also Like</h2>
                                </div>"; ?>


                                <?php  
                                    foreach ($sidebarProvider->models as $sidebarProvider) {     
                                        echo "<div class='col-lg-12 col-sm-12'><a href='details?id={$sidebarProvider->idpost}' class='portfolio-box'>";
                                        if($sidebarProvider->img==''){
                                            echo "<img src='../../img/noimages.jpg' class='img-responsive' alt='' style='width:100%;max-height:200px'>";
                                        }
                                        else{
                                            echo "<img src='../../{$sidebarProvider->img}' class='img-responsive' alt='' style='width:100%;max-height:200px'>";
                                        }
                                        echo "<div class='portfolio-box-caption'>
                                                        <div class='portfolio-box-caption-content'>
                                                            <div class='project-category text-faded'>
                                                                {$sidebarProvider->idCategory->categoryname}
                                                            </div>
                                                            <div class='project-name'>
                                                                {$sidebarProvider->subject}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div><br/>";
                                    } 
                                ?>

                                <?php echo "
                            </div>
                    </div>
                    <div class='col-md-8'>";?>

                                    <?php if(isset($done)){echo "<div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong> Thankyou !</strong> You Comment has been update to this review.</div>";}else{} ?>
                        <?php 
                        if(isset(Yii::$app->user->identity->id)){
                            if($model->id_author==Yii::$app->user->identity->id){ 

                            echo "<p style='float:left' class='btn btn-success'>{$model->status}</p>
                                <p style='text-align:right'>"; ?>

                                <?= Html::a('Update', ['update', 'id' => $model->idpost], ['class' => 'btn btn-info']) ?>
                                <?= Html::a('Delete', ['delete', 'id' => $model->idpost], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                             <?php }else{}}else{} ?>

                    <?php echo "</p>
                        <h1 class=' text-center'>{$model->subject}</h1>
                        <hr class='colored'>
                        <div style='font-size:14px'>{$model->dsc}</div>";
                            
            ?>

            

                        <?php 

                        if(isset(Yii::$app->user->identity->id)){
                            if($model->id_author==Yii::$app->user->identity->id){ 

                            echo "<p style='float:left' class='btn btn-success'>{$model->status}</p>
                                <p style='text-align:right'>"; ?>

                                <?= Html::a('Update', ['update', 'id' => $model->idpost], ['class' => 'btn btn-info']) ?>
                                <?= Html::a('Delete', ['delete', 'id' => $model->idpost], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                             <?php }else{}}else{} ?>
                                <div class='col-lg-12 col-sm-12' style='border-top:1px #F0F0F0 solid;padding:20px 0'>


                                    <!--<p>
                                        <?= Html::a('Create Comment', ['/comment/create'], ['class' => 'btn btn-success']) ?>
                                    </p>-->

                                    
                                <?php 
                                    if(isset(Yii::$app->user->identity->id)){ ?>
                                    <div class="comment-form">

                                        <?php $form = ActiveForm::begin(['id'=>$model->formName()]); $idpost = "{$model->idpost}";?>
                                        
                                        <?= $form->field($modelcomment, 'id_review')->textInput()
                                                  ->hiddenInput(['value' =>  $idpost])
                                                  ->label(false); ?>
                                                        
                                        <?= $form->field($modelcomment, 'id_usercomment')->textInput() 
                                                  ->hiddenInput(['value' => Yii::$app->user->identity->id])
                                                  ->label(false); ?>

                                        <?= $form->field($modelcomment, 'commentdesc')->textarea(['rows' => 6])
                                                  ->label(false); ?>

                                        <?= $form->field($modelcomment, 'commenton')->textInput() 
                                                  ->hiddenInput(['value' => date('Y-m-d h:m:s')])
                                                  ->label(false); ?>

                                        <div class="form-group">
                                            <?= Html::submitButton($modelcomment->isNewRecord ? 'Add Comment' : 'Update', ['class' => $modelcomment->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                        </div>

                                        <?php ActiveForm::end(); ?>

                                    </div>
                                    <?php }else{
                                        echo "<a href='".Yii::$app->urlManagerFrontEnd->createUrl('/site/login')."' class='btn btn-success'>Login to Add Comment</a>";
                                    } ?>
                                </div><div class='col-lg-12 col-sm-12' style='border-top:1px #F0F0F0 solid;padding:20px 0 50px 0'>

                                    <?= ListView::widget([
                                        'dataProvider' => $dataProvider,
                                        'itemView' => function($searchModelComment)
                                        {
                                            return "<div class='row well' style='border-bottom: 1px solid #f0f0f0;'><div class='col-lg-8 col-sm-6'><b>".$searchModelComment->idUsercomment->firstname." ".$searchModelComment->idUsercomment->lastname."</b></div><div class='col-lg-4 col-sm-6 text-right'><span style='font-size:10px'>".$searchModelComment->commenton."</span></div><div class='col-lg-12 col-sm-12' style='line-height:2;'>".$searchModelComment->commentdesc."</div></div>";
                                        }
                                        /*
                                        'filterModel' => $searchModelComment,
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],

                                            'idcomment',
                                            'id_review',
                                            'id_usercomment',
                                            'commentdesc:ntext',
                                            'commenton',

                                            ['class' => 'yii\grid\ActionColumn'],
                                        ],*/
                                    ]); ?>

                        </div>
                    </div>
            </div>
        </div>




    </section>

</div>


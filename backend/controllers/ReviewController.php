<?php

namespace backend\controllers;

use Yii;
use app\models\Review;
use backend\models\SearchReview;
use backend\models\SearchComment;
use backend\models\Comment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\rbac\DbManager;


use yii\web\UploadedFile;


/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchReview();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( Yii::$app->user->can('create-post'))
        {
            $model = new Review();


            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->id_author = Yii::$app->user->identity->id;
                $model->id_category = 1;
                $model->createon = date('Y-m-d h:m:s');

                $model->file = UploadedFile::getInstance($model, 'file');
                $save_file = '';
                if(isset($model->file)){
                    $imageid = $model->idpost;
                    $imagepath = 'uploadsimage/'; // Create folder under web/uploads/logo
                    $model->img = $imagepath .$imageid.'-'.$model->file->name;
                    $save_file = 1;
                }
        
                if ($model->save()) {
                        if($save_file){
                            $model->file->saveAs($model->img);
                            return $this->redirect(['details', 'id' => $model->idpost]);
                        }
                        else{
                            return $this->redirect(['details', 'id' => $model->idpost]);                            
                        }
                }else{
                    return $this->redirect(['details', 'id' => $model->idpost]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }   
        else
        {
            throw new ForbiddenHttpException;
            
        }
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {


                $model->id_author = Yii::$app->user->identity->id;
                $model->createon = date('Y-m-d h:m:s');
                $model->id_category = 1;

                $model->file = UploadedFile::getInstance($model, 'file');
                $save_file = '';
                if($model->file){
                    $imageid = $model->idpost;
                    $imagepath = 'uploadsimage/'; // Create folder under web/uploads/logo
                    $model->img = $imagepath .$imageid.'-'.$model->file->name;
                    $save_file = 1;
                }
        
                if ($model->save()) {
                        if($save_file){
                            $model->file->saveAs($model->img);
                            return $this->redirect(['details', 'id' => $model->idpost]);
                        }
                }else{
                    return $this->redirect(['details', 'id' => $model->idpost]);
                }

                return $this->redirect(['details', 'id' => $model->idpost]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDetails($id)
    {
        $searchModel = new SearchReview();
        $sidebarProvider = $searchModel->sidebarsearch(Yii::$app->request->queryParams);

        $searchModelComment = new SearchComment();
        $dataProvider = $searchModelComment->searchforeview($id);
        //print_r($dataProvider);
        //die();



        //add model for create comment
        $modelcomment = new Comment();

        if ($modelcomment->load(Yii::$app->request->post()) && $modelcomment->save()) {
            $done = 1;
            return $this->render('detail', [
                'model' => $this->findModel($id),
                'sidebarProvider' => $sidebarProvider,
                'searchModelComment' => $searchModelComment,
                'dataProvider' => $dataProvider,
                'modelcomment' => $modelcomment,
                'done' => $done,
            ]);
        } else {
        return $this->render('detail', [
            'model' => $this->findModel($id),
            'sidebarProvider' => $sidebarProvider,
            'searchModelComment' => $searchModelComment,
            'dataProvider' => $dataProvider,
            'modelcomment' => $modelcomment,
        ]);
        }

    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

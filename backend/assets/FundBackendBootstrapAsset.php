<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FundBackendBootstrapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/bootstrap/css/bootstrap.min.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800',
        'http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic',
        'css/bootstrap/font-awesome/css/font-awesome.min.css',
        'css/bootstrap/css/animate.min.css',
        'css/bootstrap/css/creative.css',
        'css/slider/unslider.css',
        'css/slider/unslider-dots.css'

    ];
    public $js = [
        'css/bootstrap/js/jquery.js',
        'css/bootstrap/js/bootstrap.min.js',
        'css/bootstrap/js/jquery.easing.min.js',
        'css/bootstrap/js/jquery.fittext.js',
        'css/bootstrap/js/wow.min.js',
        'css/bootstrap/js/creative.js',
        //'//code.jquery.com/jquery-2.1.4.min.js',
        'css/slider/unslider.js',
        //'css/bootstrap/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2016 at 06:16 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fundgameswithreview`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('create-comment', 1, NULL),
('create-comment', 2, NULL),
('create-post', 1, NULL),
('create-post', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'allow to create anything', NULL, NULL, NULL, NULL),
('create-comment', 1, 'allow user to add comments', NULL, NULL, NULL, NULL),
('create-post', 1, 'allow user to add a post', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'create-comment'),
('admin', 'create-post');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL,
  `categoryname` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idcategory`, `categoryname`) VALUES
(1, 'RPG'),
(2, 'Tetris');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `idcomment` int(11) NOT NULL,
  `id_review` int(11) NOT NULL,
  `id_usercomment` int(11) NOT NULL,
  `commentdesc` text NOT NULL,
  `commenton` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `idpost` int(11) NOT NULL,
  `subject` varchar(155) NOT NULL,
  `img` varchar(200) DEFAULT NULL,
  `dsc` varchar(5555) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_author` int(11) NOT NULL,
  `createon` datetime NOT NULL,
  `status` enum('Draft','Active') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`idpost`, `subject`, `img`, `dsc`, `id_category`, `id_author`, `createon`, `status`) VALUES
(1, 'Assassin’s Creed', 'uploadsimage/1-assasincreed.jpg', '<p>Say what you will about the Assassin&rsquo;s Creed Chronicles games so far: They know how to make one heck of a first impression. With India, the ominous mists that frequently shrouded the floating junks and palaces of the previous game--Assassin&#39;s Creed Chronicles: China--have lifted, and a dazzling, painterly Shangri-La awaits, awash in henna patterns and deep hues.</p>\r\n\r\n<p>Something of a pseudo-sequel to Assassin&rsquo;s Creed Chronicles: China, ACC: India picks up 300 years later. The Templars and Assassins are searching for a legendary artifact: the powerful Koh-I-Noor diamond. It&#39;s encouraging at first to visit a setting as underutilized as India, but it quickly gives way to disappointment once you realize its unique qualities are merely used for window dressing. When you&rsquo;re running, jumping, climbing, and killing in ACC: India, its typically more for its own sake than any investment in the story. To expect more is folly.</p>\r\n\r\n<p>Arbaaz&#39;s attacks can sometimes feel a little sluggish, with the response time for him to even unsheath his sword not exactly quick when there&#39;s enemies all around. If you can accomplish missions without having to resort to hand-to-hand combat, even better, but it&#39;s not easy when the appropriate icon occasionally fails to appear when you&#39;re close enough to an enemy for a stealth takedown. Trial-and-error is definitely encouraged here, and thankfully, the game reloads in a flash, allowing for very little break in the action should you need to restart. The tools of the trade remain mostly the same--smoke bombs, explosive noisemakers, a simple grappling hook, and a whistle are joined by a chakram which can be thrown to cut ropes or stun enemies. New Animus Helix powers allow for instant, on-the-fly invisibility or one-hit kills. There&#39;s even a wonderful little puzzle involving a primitive sniper rifle that&rsquo;s a bit tricky but ultimately satisfying. Still, ACC: India represents a missed opportunity to hearken back to Assassin&rsquo;s Creed&rsquo;s roots, which gave you a series of infiltration missions where you could ignore or avoid most other NPCs, and instead just focus on that big fat kill at the end. The challenge rooms that open up as you progress in the campaign scratch that itch to an extent, but not enough to satisfy it for good.</p>\r\n\r\n<p>On the flipside, there are more sequences where straight parkour is required to get out of a hectic situation, which was arguably one of the more fun, unique parts of ACC: China. This time, you&rsquo;re more likely to get into these situations because you&rsquo;re trailing villains through temples or a full-on chaotic war has broken out and you need to flee to safety. The problem here is that, for all the speed these sequences require, and as gratifying sprinting across obstacles while cutting down enemies can be,courses tend to grind to a halt for uninspired timed-jump puzzles, which ruin the breakneck pace of the rest of the courses.</p>\r\n\r\n<p>The real frustration of Assassin&rsquo;s Creed Chronicles: India is seeing a game that fails to fix the problems of its predecessor, or take advantage of its setting&#39;s great thematic potential. We have a game that takes place in a particularly bloody era of Indian (and, in one segment, Afghan) history, where the Templar villains are two high-ranking British officials, and the protagonist has every given reason to defy the Assassin Brotherhood. Yet all of the most interesting efforts to fight the good fight are reduced to finding collectibles.</p>\r\n\r\n<p>For the mainline Assassin&#39;s Creed games, the history and characters are the bones holding the gameplay upright. For the Chronicles series, the curiosities presented by the setting act as a thin veneer that only momentarily distracts from the flaws beneath. Like Arbaaz himself, these great moments have a bad habit of vanishing into thin air when you least want them to, bit there is still a lot of fun to be had in this Indian adventure.</p>\r\n', 1, 1, '2016-01-19 05:01:45', 'Active'),
(2, 'New Fallout 4 PC Beta Patch Arrives', 'uploadsimage/-fallout.png', '<p>Fallout 4&#39;s next patch is starting its rollout. Bethesda has released a beta version of update 1.3.45 on PC through Steam for people who have opted in to receive beta updates.&nbsp;</p>\r\n\r\n<p>The full Fallout 4 1.3.45 patch notes are below.</p>\r\n\r\n<p><span style="font-size:20px"><strong>Fallout 4 1.3.45 Patch Notes</strong></span></p>\r\n\r\n<p><strong>New Features</strong></p>\r\n\r\n<ul>\r\n	<li>New ambient occlusion setting, HBAO+</li>\r\n	<li>New weapon debris effects (NVIDIA cards only)</li>\r\n	<li>Added status menu for settlers in your settlements</li>\r\n	<li>Added ability to rotate an object you are holding with left/right triggers and pressing down on left thumbstick lets you switch the rotating axis</li>\r\n	<li>Improved &quot;ESDF&quot; keys remapping support while in Workshop mode</li>\r\n	<li>Gameplay Fixes</li>\r\n</ul>\r\n\r\n<p><strong>General memory and stability improvements</strong></p>\r\n\r\n<ul>\r\n	<li>Improved performance when looking through a scope</li>\r\n	<li>Fixed issue where player could warp to a different location when aiming</li>\r\n	<li>Companions can no longer get stuck with radiation poisoning</li>\r\n	<li>Fixed an issue where Vault 81 residents would not dismember correctly</li>\r\n	<li>Big Leagues perk now displays calculated damage correctly</li>\r\n	<li>Fixed issue with third person camera not displaying properly after exiting certain crafting stations</li>\r\n	<li>Fixed an issue where subtitles would occasionally not update properly</li>\r\n	<li>Effects will properly be removed on companions when items are unequipped</li>\r\n	<li>MacReady&rsquo;s Killshot perk now calculates headshot percentages properly</li>\r\n	<li>Fixed an issue with NPCs getting stuck in Power Armor</li>\r\n	<li>Fixed a rare issue with companions getting stuck in down state</li>\r\n	<li>Second rank of Aquaboy now calculates properly</li>\r\n	<li>Fixed an issue with resistance not always lowering the damage correctly when added by mods</li>\r\n	<li>Enabled number of characters available when renaming an item (XB1)</li>\r\n	<li>Fixed issue with player becoming dismembered while still alive</li>\r\n	<li>Robotics expert is now usable in combat</li>\r\n	<li>Stimpaks can now be used on Curie after the transformation</li>\r\n	<li>Playing a holotape found in wilderness while switching point of view no longer causes the screen to blur or controls to be locked</li>\r\n</ul>\r\n\r\n<p><strong>Quest Fixes</strong></p>\r\n\r\n<ul>\r\n	<li>Fixed an issue with &quot;Taking Independence&quot; where the minutemen remaining from the battle against the Mirelurk Queen would not gather in the Castle</li>\r\n	<li>Fixed an issue where invulnerable characters would get stuck in combat</li>\r\n	<li>Fixed an issue where Preston would send player to a settlement instead of a dungeon as part of a Minutemen quest</li>\r\n	<li>Fixed an issue where Synths could attack the Castle while the player was friends with the Institute</li>\r\n	<li>Fixed an issue where killing a caravan would leave a quest open</li>\r\n	<li>Fixed an issue where Dogmeat would stay at Fort Hagen after &quot;Reunions&quot; was completed</li>\r\n	<li>Fixed an issue where the player couldn&#39;t talk to Desdemona to complete &quot;Underground Undercover&quot;</li>\r\n	<li>Fixed an issue where the player could get stuck exiting the cryopod</li>\r\n	<li>Fixed an issue where the player could no longer get Preston as a companion</li>\r\n	<li>In &quot;The End of the Line,&quot; fixed an issue that would prevent the player from killing the leaders of the Railroad</li>\r\n	<li>Fixed an issue with Minutemen quests repeating improperly</li>\r\n	<li>Fixed an issue where the player couldn&#39;t get back into the Railroad headquarters after being kicked out of the Brotherhood of Steel</li>\r\n	<li>After finishing &quot;The Big Dig,&quot; fixed an issue where Hancock would no longer offer to be a companion or help with the &quot;Silver Shroud&quot; quest</li>\r\n	<li>Fixed an issue with obtaining the Dampening Coils from Saugus Ironworks before going to Yangtze</li>\r\n	<li>During &quot;Unlikely Valentine,&quot; fixed an issue where the player could be blocked from entering Vault 114</li>\r\n	<li>In &quot;Confidence Man,&quot; Bull and Gouger can now be killed</li>\r\n	<li>During &quot;Taking Independence,&quot; fixed an issue that would prevent the radio transmitter from powering up</li>\r\n	<li>In &quot;Human Error,&quot; fixed an issue where killing Dan would cause the quest to not complete properly</li>\r\n	<li>Fixed an issue with &quot;Tactical Thinking&quot; where leaving dialogue early with Captain Kells to reprogram P.A.M. could cause quest to not completely properly</li>\r\n</ul>\r\n', 1, 1, '2016-01-19 05:01:37', 'Active'),
(3, 'Just Cause 3', 'uploadsimage/3-justcause.jpg', '<p>Just Cause 3 makes no apologies for its outrageous nature. It&#39;s a power fantasy in every sense of the phrase, placing you in a world rife with destructible environments and giving you creative instruments with which to destroy them. There are intermittent technical problems, and scripted moments detract from the freedom found elsewhere, but in the end, Just Cause 3 provides a spectacular, explosive sandbox experience.</p>\r\n\r\n<p>The plot revolves around returning protagonist Rico Rodriguez, who&#39;s arrived in the fictional Republic of Medici during the height of Sebastiano Di Ravello&#39;s military dictatorship. The story here is forgettable, but delivers an effective invitation: dozens of military installations cover the world map, and it&#39;s your job to blow them up for the rebel forces.</p>\r\n\r\n<p>Rodriguez himself is a mashup of masculine action stars and comic book characters, so it makes sense that I often felt like a superhero in his shoes. By supplying you with a wingsuit, parachute, and grappling hook, Just Cause 3 gives you an effective means of transportation, as well as a smooth, nuanced traversal system.</p>\r\n\r\n<p>There is a steep learning curve, but with practice, I was leaping from helicopters, gliding through enemy bases, and floating over farmland with ease. It&#39;s thrilling to leap from a cliff, free-fall for 10 seconds, grapple to a nearby rock, and use the momentum to launch back into the air with parachute deployed. Rico actually felt like a hero learning his new skillset. It&#39;s as if Avalanche Studios combined Batman, Spider-Man, and The Punisher, and thrust its creation into a vivid Mediterranean landscape.</p>\r\n\r\n<p>What follows is a collision of spectacle and scale. Helicopters dot the sky. Explosions chain across the screen. Combining a parachute and grenade launcher transforms Rodriguez into a floating artillery battery from above. In a world teetering toward total destruction, Just Cause 3 grants you the tools to push it over the edge.</p>\r\n\r\n<p>The traditional grenades, remote mines, and numerous land, air, and sea vehicles are all on call in the rebel arsenal. Then there&#39;s the tether: this grappling hook modification attaches two separate objects, and flings them toward each other, often with hilarious results. Rodriguez can reel enemies toward explosive barrels, collapse watchtowers, and pull attack helicopters into a fiery end. It&#39;s a testament to this game&#39;s creativity that guns were my last resort.</p>\r\n\r\n<p>There&#39;s a sequence in Just Cause 3 in which a fleet of helicopters pursue you over a mountain range. In any other game, I may have resorted to the RPG slung across my back. But in keeping with this game&#39;s lack of convention, I grappled to the nearest attack chopper, pulled the pilot out, and assumed control in his place.</p>\r\n\r\n<p>When Just Cause 3 is consistent, however, it&#39;s a stunning display of cause and effect, as watchtowers topple into fuel tanks, which blow up nearby helicopters, which sail into oncoming vehicles. I often spent hours setting up outlandish chain reactions, or trying new gear mods, knowing full well I wasn&#39;t making any progress in the traditional sense. I was content to just sit back and marvel as it all happened.</p>\r\n\r\n<p>But there&#39;s a more thoughtful undercurrent as well. Despite the explosions and instant gratification throughout, Just Cause 3 also encourages experimentation and foresight, planning and careful approaches. The results are as rewarding as they are entertaining.</p>\r\n\r\n<p>Editor&#39;s Note: The majority of our time with Just Cause 3 was spent with the PC version, followed by several hours on both PS4 and Xbox One. Based on the review builds provided, the game performed better on PC, with higher and more stable frame rates, fewer bugs, and better looking environments. However, the problems did not affect the overall experience enough to impact individual scores.</p>\r\n', 1, 1, '2016-01-19 06:01:20', 'Active'),
(4, 'Minecraft: Story Mode Episode Three', 'uploadsimage/-minecraft.jpg', '<p>Endermen were already the creepiest creatures in the world of Minecraft, but the latest episode in Telltale&#39;s narrative take on the hit game makes them close to something truly disturbing. In Minecraft Story Mode: The Last Place You Look, these enigmatic creatures take center stage, proving to be a scary antagonists in several key sequences. Their use here is emblematic of Minecraft Story Mode&#39;s clever approach to creating a narrative within the wider world of Minecraft; there&#39;s a real reverence for the original game on display, and while the adherence may be strict, the way the many disparate pieces of Minecraft have been put together in service of an increasingly compelling story continues to be engaging.</p>\r\n\r\n<p>This third episode of Minecraft Story Mode features the same propulsive energy as the second, with the added bonus of also being the most thrilling narratively. By the end of this short chapter, the stakes for the entire series have not only been elevated but, in many ways, completely reset as well.</p>\r\n\r\n<p>Episode Two ended on a somewhat lame cliffhanger, and it&#39;s not long before our group of heroes manage to find their way out of their predicament by doing the very Minecraft thing of just digging straight down. From here, The Last Place You Look moves briskly, skipping from an exciting opening sequence set within a gigantic mob grinder filled with enemies to a visit to the End to find the last missing member of the Order of the Stone (voiced by the always excellent John Hodgman).</p>\r\n\r\n<p>Along the way, some of the series&#39; key relationships and plot lines are moved forward, some quite significantly. Lukas&#39; growing unease threatens to boil over, your character&#39;s relationship with your Wither sickness-infected party member--whose identity is dependant on a choice you made in Episode 1--gains complexity, and the team&#39;s overarching goal of coming up with an explosive solution to defeat the gigantic Wither destroying the world comes to a head.</p>\r\n\r\n<p>Narratively, this is the most complex Minecraft Story Mode has been so far, which is a welcome change from the simpler, black-and-white shading of previous episodes. It&#39;s also the most emotionally wrought. Several scenes were quite affecting, and was surprising given the series&#39; fairly light and jovial tone so far. While Episode Three is still light years away from the rawness of Telltale&#39;s take on The Walking Dead or Game of Thrones, younger players may need some guidance with some of the heavier situations that occur.</p>\r\n\r\n<p><br />\r\nEpisode Three also ramps up the tension, and it&#39;s all thanks to those creepy Endermen. Their cold stares, propensity to teleport suddenly, and open- jawed screams are used to great effect here. All of the sequences where hero Jesse and the crew having to contend with Endermen are clear highlights.</p>\r\n\r\n<p>It all leads to a conclusion where, while not completely surprising in its details, is still presented in an exciting, involving way that makes you feel--almost--like resolution is close at hand. There&#39;s also a last-gasp character revelation that makes for Story Mode&#39;s first truly intriguing cliffhanger. The ending made me long for that next episode to come quicker.</p>\r\n\r\n<p>As always, Minecraft Story Mode&#39;s love for the source material shines through, in touches both large and small. Seeing a gigantic, complex grinder in action made me regret never having the time or skills to make a machine like that in my own Minecraft game, while little character touches (like everyone lowering their eyes so as not to attract Endermen attention) shows off the series&#39; consistently wonderful attention to detail.</p>\r\n\r\n<p>Minecraft Story Mode&#39;s third episode is the best so far, weaving action and story in a tight, focused package. This is another short episode (my first playthrough clocked in at less than 90 minutes), which makes Story Mode a little on the brief side compared to recent Telltale offerings. But that brevity is my biggest complaint, which, if you look at it from the most charitable view, means the game never outstayed its welcome. Story Mode remains a great experience--especially if you&#39;re playing with younger fans of Minecraft--and my anticipation for the next episode remains high.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 1, '2016-01-19 06:01:42', 'Active'),
(5, 'Skyshine''s Bedlam', 'uploadsimage/-bedlam.jpg', '<p>If you took my favorite things and threw them in a blender it would come out looking like Skyshine&rsquo;s Bedlam. Its blend of Mad Max&#39;s aesthetic, turn-based strategy combat, and roguelike trappings made me think I&rsquo;d found the perfect game for me. But just like love at first sight, once I got to know Bedlam a little better, I realized that looks can be deceiving.</p>\r\n\r\n<p>In Skyshine&rsquo;s Bedlam, you helm a vehicle known as the Dozer. The minute story details change from playthrough to playthrough because of the procedurally-generated nature of the game, but the main goal is to flee your home of Bysantine and drive you and your passengers to the Aztec City--Bysantine is suffering under the tyranny of a crazed warlord, King Viscera (no, not the ex-WWE wrestler). Along the way, you&#39;re stopped by wanderers, hostile factions, and other events and people that prevent you from just making a bee-line to your destination.</p>\r\n\r\n<p>Combat is a turn-based affair where you fight groups of marauders, mutants, and cyborgs. Your entire party can perform two actions per turn, which can be used to either move around, or to attack an enemy; for example, if you move one character and attack with another, your team&#39;s turn is over. This is far too restrictive in practice, and you often feel like there are only two strategies: move your character out of harm&#39;s way, or kill one enemy and have one of your characters die because you couldn&#39;t get them to safety.</p>\r\n\r\n<p>Simple tactics, like using a shotgun to knockback enemies into range of another character&#39;s attack, feel rewarding, but those moments are fleeting. There&#39;s not much strategic depth, and other than move distance, attack range, and damage, there&#39;s little else that distinguishes one character class from another, and little else to toy with during combat. I ultimately felt lucky--not accomplished--when I was victorious.</p>\r\n\r\n<p>The comic book-like presentation is great, and there&#39;s some enjoyable animations to behold, especially during combat. Killing an enemy with certain weapons, such as puke or a shotgun, causes them to crumble into a pile of bones. Characters perform finishing move animations if you kill an enemy point blank, like kicking them in the balls and delivering an elbow drop. These are pretty satisfying to watch and make some of the shots look absolutely devastating.</p>\r\n\r\n<p>Normally, you organically find Elites during your journey, but in the new Challenge mode, you start with a random crew of Elites and stronger-than-average soldiers from the get-go. Here, your goal is reversed: you travel from Aztec City to Byzantine, though King Viscera and factions are still on high alert, now with increased health and strength. This puts you past the stage of leveling up your crew and puts you right into the middle of hard-hitting combat. This is initially thrilling, but it can also be frustratingly difficult; you often feel forced to focus all of your resources on boss characters, which often leaves you empty handed, and ill-prepared to face the rest of your opponents.</p>\r\n\r\n<p>Skyshine&rsquo;s Bedlam has some good moments, but the story is devoid interesting or layered tales. The experience is largely defined by chasing simple goals while enduring repetitive dialogue and narrative beats along the way. Combat feels good enough, but there&#39;s not much to it, and little to strive for. Not being able to make meaningful, permanent progress is ultimately Skyshine&#39;s Bedlam&#39;s biggest flaw, and when something so crucial is missing, it&rsquo;s hard to recommend. There is some fun to be had, but Skyshine&#39;s Bedlam ultimately mirrors the reality it depicts. You can survive hardships, but only if you struggle through them.</p>\r\n', 1, 1, '2016-01-19 06:01:03', 'Active'),
(6, 'Rainbow Six Siege', 'uploadsimage/-RainboxSixSiege.jpg', '<p>The average Rainbow Six Siege multiplayer match contains a surprisingly small amount of shooting. Gunplay is, of course, still central to the Siege experience, but there&#39;s so much more to it. You&#39;ll spend just as much time strategizing with your teammates, carefully laying traps, reinforcing destructible walls, and feeling your heart race as the dull, distant rumble of your enemies&#39; breach charges suddenly gives way to intense and immediate chaos. And that&#39;s just on defense.</p>\r\n\r\n<p>Few modern shooters can match the heart-pounding exhilaration and immense strategic depth Siege achieves with its asymmetrical PvP. With no respawns, no regenerating health, and only five players per team, every life suddenly feels meaningful and precious (though you can still monitor security cameras and communicate with your team in death). Running-and-gunning will almost certainly land you on the sidelines, so you&#39;re much better off using your drivable drone to scout ahead or coordinating with your teammates to ensure all sightlines are covered.</p>\r\n\r\n<p>Being an always-online game, Siege also comes with it&#39;s fair share of minor annoyances that, while mostly unobtrusive, are still worth mentioning. Map rotation could be more consistent. Console players could use more in-game communication tools beyond the temporary marker icon. Matchmaking needs to be just as smooth on PC as it currently is on consoles. Purchasing and equipping new gear shouldn&#39;t require players to back out of matches. There needs to be an easier way to kick and report disruptive players.</p>\r\n\r\n<p>Everything from the strength of your internet connection to the makeup of your team can impact your enjoyment of Siege, but importantly, Siege itself does everything it can to ensure you&#39;re able to enjoy the game in spite of these variables. Across all the hours I spent online, players were consistently cooperative and communicative, and to some degree, I have to credit Siege&#39;s tutorials and situations for adequately conveying how the game is meant to be played.</p>\r\n\r\n<p>My experiences weren&#39;t always perfect, but when Siege works, there&#39;s nothing else like it. It&#39;s not designed to appeal to all players, and that&#39;s exactly what allows it to be something special. With so much strategic depth, those periods between firefights actually become some of the most rewarding, while firefights themselves are made all the more intense by the knowledge that you&#39;re fighting for your life, not just your kill/death ratio.</p>\r\n', 1, 1, '2016-01-19 06:01:17', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Kuncoro', 'Wicaksono', 'coroowicaksono', '55peFIV5QHPqnG4Czu6BJh_hRGqDIuWN', '$2y$13$De8GHLB1Tpp8KeW/5yzeHeEesq/P1esa0.WNeARVTib4Gx0qnI0fe', NULL, 'kuncoro.wicaksono@gmail.com', 10, 1453222158, 1453222158),
(2, 'Adi', 'Baroto', 'adibaroto', 'oVaOTKCh0Namd-x0S0KIbD1QYe71jF_F', '$2y$13$icxQkn5l8quGUPGA8uhhleyOFBFepvABqfhcLjN6Czlg3uoArEH0S', NULL, 'adi.baroto@gmail.com', 10, 1453223427, 1453223427);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idcategory`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`idcomment`),
  ADD KEY `id_usercomment` (`id_usercomment`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`idpost`),
  ADD KEY `id_author` (`id_author`),
  ADD KEY `id_category` (`id_category`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `idcomment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `idpost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`id_usercomment`) REFERENCES `user` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`id_author`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
